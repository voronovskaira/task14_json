package model;

import java.util.Comparator;

public class GemValueComparator implements Comparator<Gem> {
    public int compare(Gem o1, Gem o2) {
        if (o1.getValue() < o2.getValue()) return -1;
        if (o1.getValue() > o2.getValue()) return 1;
        return 0;
    }
}
