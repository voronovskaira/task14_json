package parser;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Gem;
import model.GemValueComparator;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JacksonParser {
    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        //JSON file to Java object
        File file = new File("src/main/resources/json/gems.json");
        List<Gem> myObjects = mapper.readValue(file, new TypeReference<List<Gem>>() {
        });
        for (Gem g : myObjects) {
            System.out.println(g + "\n");

        }
        myObjects.stream().sorted(new GemValueComparator()).forEach(System.out::println);

    }
}
